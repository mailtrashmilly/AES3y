%=============================================================
% CLT_CRITERIA : Verifica lo stato di sforzo in un laminato in base
%                applicando i criteri di massima deformazione e di Tsai-Hill
%=============================================================
% NOTA: QUESTO SCRIPT E' SVILUPPATO SOLO PER LAMINATI CON LAMINE DELLO STESSO TIPO
%=============================================================
%=============================================================
% PARAMETRI: MAX EPS E MAX SIG PER L'IMPLEMENTAZIONE DEI CRITERI
%=============================================================
%----------------------------------------------------
% Valori per:
% UD Carbonio
%----------------------------------------------------
% Deformazioni ammissibili (mm/mm)
%----------------------------------------------------
XEC=-0.005;
XET=0.0083;
YEC=-0.004;
YET=0.003;
SG=0.008;
%----------------------------------------------------
% Sforzi ammissibili (MPa)
%----------------------------------------------------
XC=-750;
XT=1245;
YC=-40;
YT=30;
S=52;
%
%=============================================================
% INPUT: Richiede il file binario in cui CLT_SOLVE ha memorizzato
%        Lo stato di sforzo
%=============================================================
StatoSigEps=input('Nome File Stato di sforzo (senza estensione) :','s');
load(StatoSigEps);
%----------------------------------------------------
% CRITERIO MASSIMA DEFORMAZIONE
%----------------------------------------------------
for (lamina=1:L.N)
    if (EPS(1,lamina)>=0.0)
       XE=XET;
    else
       XE=XEC;
    end
    if (EPS(2,lamina)>=0.0)
       YE=YET;
    else
       YE=YEC;
    end
    MAX_EPS(1,lamina)=EPS(1,lamina)/XE;
    MAX_EPS(2,lamina)=EPS(2,lamina)/YE;
    MAX_EPS(3,lamina)=EPS(3,lamina)/SG;
end
MAX_EPS
%----------------------------------------------------
% CRITERIO TSAI-HILL
%----------------------------------------------------
for (lamina=1:L.N)
    if (SIG(1,lamina)>=0.0)
       X=XT;
    else
       X=XC;
    end
    if (SIG(2,lamina)>=0.0)
       Y=YT;
    else
       Y=YC;
    end
    TSAI_HILL(lamina)=(SIG(1,lamina)/X)^2-(SIG(1,lamina)*SIG(2,lamina)/X^2)+ ...
                       (SIG(2,lamina)/Y)^2+(SIG(3,lamina)/S)^2;
end
TSAI_HILL
