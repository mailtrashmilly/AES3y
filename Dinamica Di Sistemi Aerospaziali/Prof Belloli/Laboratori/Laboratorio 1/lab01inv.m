% Copyright (C) 2009
% 
% Pierangelo Masarati <masarati@aero.polimi.it>
% 
% Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
% via La Masa, 34 - 20156 Milano, Italy
% http://www.aero.polimi.it/
% 
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation (version 2 of the License).
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

% Dinamica di Sistemi Aerospaziali
% Laboratorio informatico 1

%% Cinematica inversa

close all;
clear all;
clc;

global t1 C1 C2

% carico i dati, comuni a tutti i problemi
lab01_data;

% costruisco legge oraria moto dell'estremo libero

% xpp = ...
% ypp = ...

% xp = ...
% xp = ...

% x = ...
% x = ...

figure;
plot(t1, x, t1, y);
legend('x', 'y');
xlabel('Time, s');
ylabel('Displacement, m');
title('Imposed displacement');

figure
plot(x, y)
xlabel('x, m');
ylabel('y, m');
title('Imposed displacement');
axis equal

figure;
plot(t1, xp, t1, yp);
legend('x''', 'y''');
xlabel('Time, s');
ylabel('Velocity, m/s');
title('Imposed velocities');

figure;
plot(t1, xpp, t1, ypp);
legend('x''''', 'y''''');
xlabel('Time, s');
ylabel('Acceleration, m/s^2');
title('Imposed accelerations');

% cinematica inversa
n = length(t1);
tol = 1e-9;
imax = 100;
theta = zeros(n, 2);
thetap = zeros(n, 2);
thetapp = zeros(n, 2);

theta(1, :) = [0, 0];
% thetap(1, :) = ?
% thetapp(1, :) = ?
for i = 2:length(t1),
	if (i == 2),
		% perturbazione:
		% cambiando il vettore delle condizioni iniziali
		% si realizzano diverse soluzioni
		tt = [0; 1e-1];
		% tt = [1e-1; 0];
	else 
		tt = theta(i - 1, :)';
	end
	ttest = tol + 1;
	for j = 1:imax,
		f = [x(i); y(i)] - lab01inv_nr(tt);
		ttest = norm(f);
		if (ttest <= tol),
			disp(sprintf('step %d converged in %d iterations (test=%f)', i, j, ttest));
			break;
		end

		J = lab01inv_nr(tt, 1);
		dtt = J\f;
		tt = tt + dtt;
	end
	theta(i, :) = tt';

	f = [xp(i); yp(i)];
	thetap(i, :) = (J\f)';

% 	f = ...
	thetapp(i, :) = (J\f)';
end

% nota: per continuazione, pongo la velocit\'a all'istante iniziale
% uguale a quella al passo successivo
thetap(1, :) = thetap(2, :);

theta1 = theta(:, 1);
theta2 = theta(:, 2);
thetap1 = thetap(:, 1);
thetap2 = thetap(:, 2);
thetapp1 = thetapp(:, 1);
thetapp2 = thetapp(:, 2);

t_theta1 = theta1;
t_theta2 = theta2 - theta1;
t_thetap1 = thetap1;
t_thetap2 = thetap2 - thetap1;
t_thetapp1 = thetapp1;
t_thetapp2 = thetapp2 - thetapp1;


figure;
plot(t1, t_theta1*180/pi, t1, t_theta2*180/pi);
legend('\theta_1', '\theta_2');
xlabel('Time, s');
ylabel('Joint rotation, deg');
title('Computed joint rotation');

figure;
plot(t1, t_thetap1*180/pi, t1, t_thetap2*180/pi);
legend('\theta_1''', '\theta_2''');
xlabel('Time, s');
ylabel('Joint angular velocity, deg/sec');
title('Computed joint angular velocity');

figure;
plot(t1(1:end-1), t_thetapp1(1:end-1)*180/pi, t1(1:end-1), t_thetapp2(1:end-1)*180/pi);
legend('\theta_1''''', '\theta_2''''');
xlabel('Time, s');
ylabel('Joint angular acceleration, deg/sec^2');
title('Computed joint angular acceleration');

%% Confronto con soluzione analitica... 



%% Dinamica diretta e inversa

% dinamica inversa
% C2 = ...
% C1 = C2 + ...

figure;
plot(t1(1:end-1), C1(1:end-1), t1(1:end-1), C2(1:end-1));
legend('C_1', 'C_2');
xlabel('Time, s');
ylabel('Joint torque, Nm');
title('Computed joint torque');

% dinamica diretta (verifica)
opt = odeset('RelTol', 1e-6, ...
        'AbsTol', 1e-6, ...
        'InitialStep', 0.01, ...
        'MaxStep', 0.01);
X0 = [theta1(1), theta2(1), thetap1(1), thetap2(1)];
T = [0, 10];
[td, X] = ode45(@lab01dir_func, T, X0, opt);

figure;
plot(t1, t_theta1*180/pi, t1, t_theta2*180/pi, td, X(:, 1)*180/pi, td, (X(:, 2)-X(:, 1))*180/pi);
legend('\theta_1 computed', '\theta_2 computed', '\theta_1 verified', '\theta_2 verified');
xlabel('Time, s');
ylabel('Joint rotation, deg');
title('Computed and verified joint rotation');

