% Copyright (C) 2009
% 
% Pierangelo Masarati <masarati@aero.polimi.it>
% 
% Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
% via La Masa, 34 - 20156 Milano, Italy
% http://www.aero.polimi.it/
% 
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation (version 2 of the License).
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

% Dinamica di Sistemi Aerospaziali
% Laboratorio informatico 1

% dinamica diretta

function ydot = lab01dir_func(tvar, yvar)

lab01_data;

global t1 C1 C2

theta1 = yvar(1);
theta2 = yvar(2);
thetap1 = yvar(3);
thetap2 = yvar(4);

t_theta1 = theta1;
t_theta2 = theta2 - theta1;

% theta1' = thetap1
% theta2' = thetap2
% thetap1' = ...
% thetap2' = ...

ydot = zeros(size(yvar));
ydot(1) = thetap1;
ydot(2) = thetap2;

% interpolo linearmente le coppie
i = find(t1 > tvar);
i = i(1) - 1;

dt = t1(i + 1) - t1(i);
w1 = (t1(i + 1) - tvar)/dt;
w2 = (tvar - t1(i))/dt;

c1 = C1(i)*w1 + C1(i + 1)*w2;
c2 = C2(i)*w1 + C2(i + 1)*w2;

% matrice di massa
% M = ...

% termine noto
% f = ...
    
ydot(3:4) = M\f;

