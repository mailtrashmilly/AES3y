function xp = dinfun(t, x)
    global Mc Ja Ma d a b L g tF dtF Fc_sweep
    
    xp = zeros(2,1);
    
%     risposta a gradino in t = 0.2 s

    F = 0;
    Fc = 0;

    if t > 0.2 
        Fc = 10;
%         F = 50;
    end

% %     sweep in frequenza
%     
%     F = 0;
%     Fc = Fc_sweep(abs(tF - t) < dtF/2); 
    

    dot_alpha = x(1);
    alpha = x(2);
    beta = asin(-(d + a*sin(alpha))/b);
    beta = pi - beta;
    
    % calcoliamo una sola volta le quantita' trigonometriche:
    cos_b = cos(beta);
    sin_b = sin(beta);
    tan_b = sin_b/cos_b;
    cos_a = cos(alpha);
    sin_a = sin(alpha);
    
    dot_beta = -(a/b)*(cos_a/cos_b)*dot_alpha;
    
    % costruiamo i 'pezzi' che ci servono:
    A = -b*(cos_a + tan_b*sin_b);
    B =  a*(cos_a - sin_a*tan_b);
    C =  a*(sin_a + cos_a*tan_b);
    
    Jrid = Mc*C^2 + Ja + Ma;
    % ricordando che xp(1) = ddot_alpha e xp(2) = dot_alpha, scriviamo la
    % derivata rispetto al tempo del vettore di stato: 
    xp(1) = 1/Jrid*(-Mc*(B*C*dot_alpha^2 + A*C*dot_beta^2) - F*2*L*sin_a + Ma*g*L*cos_a - Fc*C);
    xp(2) = x(1);
  
end