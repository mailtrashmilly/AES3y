function [M,dM] = energia_cinetica(v1,v2,v3,om1,om2,theta)

% Termine non lineare energia cinetica

global L R
global m1 m2 m3 J1 J2

M = m1*v1^2+m2*v2^2+m3*v3^2+J1*om1^2+J2*om2^2;

dv2 = 8*sin(theta)*cos(theta)*(L/R)^2;

dom2 = 8*sin(theta)*cos(theta)*L^2;

dM = 2*m2*dv2+2*J2*dom2;
