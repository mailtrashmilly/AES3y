clear
close
clc

%Equilibrio

%Dati
g=9.81;
L=1;
R=0.2;
m1=5;
m2=3;
m3=2;
J1=(1/12)*m1*(2*L)^2;
J2=m2*R^2;
k=500;
r=5;

a=+m1*g+4*m3*g+0.5*m1*g*sqrt(2)+2*m3*g*sqrt(2);