format long
R_ex = 100000;
R = [99866 99872 99871 99830 99821 99780 99817 99968 99701 99816]';
R_avg = mean(R)

err = R - R_avg
errAvg = 100*abs(R_ex-R_avg)/R_ex

stDev = std(R)
variance = stDev^2
