% INTEGRAZIONE DELL'EQ. DI VON KARMAN SUL SEMICORPO DI RANKINE:
% METODO DI THWAITES

clear all
close all
clc

%Dati iniziali
U=1;
m=0.1; %->Intensità sorgente
nu=1.5*10^(-5);
mu=1.81*10^(-5);
ppi=2*pi;

x_s=-m/(ppi*U); %-->ascissa del punto di ristagno

%Discretizzazione spaziale
% se N_x e N_y sono pari i vettori di coordinate non contengono l'origine
N_x=500;  
N_y=500;
xmin=3*x_s;
xmax=-20*x_s;
ymin=4*x_s;
ymax=-4*x_s;
x=linspace(xmin,xmax,N_x);
y=linspace(ymin,ymax,N_y);

% Corrente attorno ogiva cilindrica semi-infinta di Rankine
%Linee isopotenziali

for i=1:N_x

     for j=1:N_y
         
        phi(j,i)=U*x(i)+m/ppi*log(sqrt(x(i)^2+y(j)^2));  
        
     end

end

figure
contour(x,y,phi,20);
xlabel('x');
ylabel('y');
title('Linee isopotenziali')
axis equal

%Linee di corrente

for i=1:N_x

	for j=1:N_y

	        psi(j,i)=U*y(j)+m/ppi*atan2(y(j),x(i));   
end

end
%psi = psi .* (psi>m/2 | psi<-m/2); %-->solo linee "esterne" al corpo
figure
contour(x,y,psi,20);
xlabel('x');
ylabel('y');
title('Linee di corrente')
axis equal
 
% psi_c = m/2 identifica la linea di corrente passante per il punto di ristagno
% e corrispondente alla superficie del semicorpo

psi_c=m/2;
figure
contour(x,y,psi,[psi_c,psi_c]);
xlabel('x');
ylabel('y');
hold on
contour(x,y,psi,[-psi_c,-psi_c]);
axis equal
title('Linee di corrente nel punto di ristagno')

% disegno parte superiore del corpo
jj=1;

x_c(1)=x_s;
y_c(1)=0;

for j=1:N_y
    
	if y(j)>0
        
		x1 = y(j)/tan(pi+y(j)/x_s);
      
		if (x1<= xmax) && (x1>=xmin)
        
		   jj=jj+1;
           x_c(jj)=x1;
           y_c(jj)=y(j);
      
	    end
    
	end

end
	
nmax_c=jj;

figure
plot(x_c,y_c);
xlabel('x');
ylabel('y');
axis equal
title('Parte superiore del semicorpo')

% s: coordinata curvilinea con origine nel punto di ristagno e nel
% semipiano y>0

for i=1:nmax_c-1
    
   ds(i)=sqrt((x_c(i+1)-x_c(i))^2+(y_c(i+1)-y_c(i))^2);

end

s(1)=0;

for i=2:nmax_c
    
   s(i)=s(i-1)+ds(i-1);

end

%Calcolo della velocità esterna allo strato limite

u_x_c= U+m/ppi.*x_c./(x_c.^2.+y_c.^2);
u_y_c= m/ppi.*y_c./(x_c.^2.+y_c.^2);

quiver(x_c,y_c,u_x_c,u_y_c)
xlabel('x');
ylabel('y');
axis equal
title('Velocità sul corpo')

u_mod_c_2=u_x_c.^2 + u_y_c.^2;
c_p= 1 - u_mod_c_2/U^2;

Ue(2:nmax_c)=sqrt(u_mod_c_2(2:nmax_c));
Ue(1)=0;


figure
hold on
plot(s,Ue,"linewidth",2)
plot(s,c_p,"m","linewidth",2)
legend('U','C_p')
xlabel('s');

% condizioni al punto di ristagno per un corpo arrotondato

Udot(1)=Ue(2)/ds(1);
c1=0.075;
theta(1)=sqrt(c1*nu/Udot(1));
Ue5int=0;


% integrazione

for i=2:nmax_c
 
   Udot(i)=(Ue(i)-Ue(i-1))/ds(i-1);
   Ue5int=Ue5int+(0.5*(Ue(i)+Ue(i-1)))^5*ds(i-1);
   theta(i)=sqrt(0.45*nu/Ue(i)^6*Ue5int);
    
  % parametro di Thwaites
	K(i)=theta(i)^2*Udot(i)/nu;
  
	if(K(i)>0 && K(i)<0.1)
    % formule per H e l (oppure H e cf) 
		H(i)=2.61-3.75*K(i) + 5.24*K(i)^2;    
		l(i)=0.22 +1.57 *K(i) -1.8 *K(i)^2;
  
	elseif(K(i)>-0.1 && K(i)<0)
    
		H(i)=2.088 + 0.0731/(K(i)+0.14);   
		l(i)=0.22+1.402*K(i) + 0.018 *K(i)/(K(i)+0.107);
  
	end

end

delta=H.*theta;
tauw=mu*l.*Ue./theta;

%Rappresentazione parametro di Thwaites
figure
plot(s(1:nmax_c),K(1:nmax_c),"linewidth",2)
xlabel('s');
ylabel('K')
title('Parametro di Thwaites')

%Rappresentazione spessori strato limite
figure
hold on
plot(s(2:nmax_c),delta(2:nmax_c),"linewidth",2)
plot(s(2:nmax_c),theta(2:nmax_c),"m","linewidth",2)
xlabel('s');
legend('$\delta$','$\theta$','interpreter','latex')
title('Spessori strato limite')

%Rappresentazione sforzo a parete
figure
plot(s(1:nmax_c),tauw,"linewidth",2)
xlabel('s');
ylabel('$\tau_w$','fontsize',14,'interpreter','latex')
title('Sforzo a parete')
